# SELENIUM PROJECT

## DEVELOPER INFO

**E-MAIL**: marusha@list.ru

**SITE**: www.kuleshova.ru

## SOFTWARE REQUIREMENT 

**JAVA**: OPENJDK 1.8

**OS**: WINDOWS 10 Pro

## HARD REQUIREMENT 

**CPU**: i7-7500U

**RAM**: 16 ГБ

**SSD**: 512 ГБ

## BUILD APPLICATION 
```
mvn clean install
```
## RUN TESTS 
```
mvn test
```
## RUN APPLICATION 
```
java - jar ./selenium-1.0.0.jar
```
