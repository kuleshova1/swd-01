package ru.kuleshova.selenium;

import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    private Calculator calculator = new Calculator();
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void test(){
        Assert.assertEquals(0, calculator.sum(-1,1));
        Assert.assertEquals(-2, calculator.sum(-1,-1));
        Assert.assertEquals(2, calculator.sum(1,1));
    }
}
