package ru.kuleshova.selenium;

import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ){
        System.out.println( "CALCULATOR" );
        final Scanner scanner = new Scanner(System.in);
        System.out.println( "Enter A:" );
        final int a = scanner.nextInt();
        System.out.println( "Enter B:" );
        final int b = scanner.nextInt();
        final Calculator calculator = new Calculator();
        System.out.println(calculator.sum(a,b));
    }
}
